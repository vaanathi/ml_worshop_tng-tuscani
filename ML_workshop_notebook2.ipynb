{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### TNG-TUSCANI ML Workshop Session 3, Monday 10th December 2018\n",
    "\n",
    "# Introduction to Machine Learning 3\n",
    "\n",
    "Hello again!  Welcome to the Jupyter Notebook for the TNG-TUSCANI Machine Learning workshop session 3.  By the end of this session you should be able to work with (medical) images in a Python-like environment in order to apply some basic Machine Learning (ML) techniques or prepare them for a Deep Learning (DL) model.\n",
    "\n",
    "To start, try executing the following Notebook cell by selecting it and then pressing the Shift and Enter keys:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"Hello again, Tuscany!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So, what are we going to do today?  Well, we're going to look at some medical images - specifically, some brain scans.  Thinking back to our generic project plan structure, our \"big picture\" (so to speak) is:\n",
    "\n",
    "\"_We would like to apply a simple ML technique to some T2-weighted FLAIR MRI scans to see if we can identify White Matter Lesions (WMLs)_.\"\n",
    "\n",
    "We have provided some real (and anonymised) sample data in `data/Segmentation`.\n",
    "\n",
    "\n",
    "## Prepare your toolbox\n",
    "\n",
    "So first, let us prepare our toolbox - the same packages as before, with a few extra for playing with images:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# If you set up your environment last week, there may be some additional modules to install with\n",
    "# Conda - please see the new version of the README.md file for more information or if you have\n",
    "# problems.\n",
    "\n",
    "# For general Operating System-related stuff.\n",
    "import os\n",
    "\n",
    "# For the plotting.\n",
    "import matplotlib.pyplot as plt\n",
    "%matplotlib inline\n",
    "\n",
    "# Numpy is the package that lets us do mathsy stuff...\n",
    "import numpy as np\n",
    "\n",
    "# pandas provides nice wrapper classes for our data...\n",
    "import pandas as pd\n",
    "\n",
    "# ...and seaborn lets us do pretty plots.\n",
    "#import seaborn as sns\n",
    "\n",
    "# Make the default plotting EVEN MORE pretty.\n",
    "# We like pretty plots.\n",
    "#sns.set()\n",
    "\n",
    "# NiBabel is a Python package for handling Nifti files, a standard image format\n",
    "# for MRI scans - so a useful toolkit to be familiar with!\n",
    "import nibabel as nib\n",
    "\n",
    "# This little gem makes handling file names and directories simpler...\n",
    "import glob\n",
    "\n",
    "# For splitting data into training and test sets.\n",
    "from sklearn.model_selection import train_test_split\n",
    "\n",
    "# For clustering with scikit-learn.\n",
    "from sklearn import cluster\n",
    "\n",
    "# For Support Vector Machines with scikit-learn.\n",
    "from sklearn import svm\n",
    "\n",
    "# For sparse matrices.\n",
    "from scipy.sparse import coo_matrix"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Get your data\n",
    "\n",
    "What data do we have?  Let's look in the `data/Segmentation` folder.  To run an external command like `ls` in a Jupyter Notebook, you can preceed the command with an exclamation mark and it will run as if it were in the terminal from which you are running the notebook.  Try the following:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Print the current working directory (i.e. where the notebook is running):\n",
    "!pwd\n",
    "# List the files and folders in the current directory\n",
    "!ls -l"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Now list the contents of data/Segmentation\n",
    "!ls -l data/Segmentation/"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "OK, so it looks like we have data for five subjects!  And what do we have for each subject?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!ls -l data/Segmentation/subj1/"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Without documentation, or knowing that the naming conventions follow some sort of standard (like the Brain Imaging Data Structure, [BIDS](http://bids.neuroimaging.io/)), you'd need to ask whomever provided the data what each of these files represents.  Fortunately, we can tell you:\n",
    "\n",
    "* `T2_brain.nii.gz` is a Nifti file with a 3D T2-weighted FLAIR volume of the brain (brain extracted);\n",
    "* `T1_brain_to_T2.nii.gz` is a Nifti file with the matching 3D T1-weighted image, registered to the T2 FLAIR;\n",
    "* `T2_mask.nii.gz` is a Nifti file with the White Matter Lesion (WML) masks.\n",
    "\n",
    "We're going to try and teach a ML model to spot the WMLs, using the scans (the data) and the WML masks (the labels)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Look at the data\n",
    "\n",
    "Looking at your data is, as always, very important - but also a lot easier when you're dealing with images.  If you've got a Nifti file viewer installed on your system, such as `fsleyes`, we'd recommend opening some of the images and looking at them using that (like we did with Excel in the last session).\n",
    "\n",
    "However, there are also some things we can do in Python using the NiBabel package to handle the Nifti files:\n",
    "\n",
    "http://nipy.org/nibabel/"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load the images into Nifti files:\n",
    "\n",
    "## The path to subject 4's data - picked at \"random\".\n",
    "subj1_path = \"./data/Segmentation/subj4/\"\n",
    "\n",
    "## The T2 FLAIR for subject 4.\n",
    "t2_img = nib.load(os.path.join(subj1_path, \"T2_brain.nii.gz\")).get_data().astype(float)\n",
    "\n",
    "## The T1 (registered to the T2) brain for subject 4.\n",
    "t1_img = nib.load(os.path.join(subj1_path, \"T1_brain_to_T2.nii.gz\")).get_data().astype(float)\n",
    "\n",
    "## The WML mask for subject 4.\n",
    "wl_msk = nib.load(os.path.join(subj1_path, \"T2_mask.nii.gz\")).get_data().astype(int)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can use `matplotlib`'s `imshow()` method to plot slices from our Nifti file to see what's going on.  Note that there are Python libraries like NiLearn that provide some nice neuroimaging-focussed methods that will do this sort of thing for you, but we're keeping it \"simple\" here.  Don't worry too much about what each command in the following cell is doing - there's some looping and zipping jiggery-pokery to reduce the amount of code we need to write."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.close('all')\n",
    "fig = plt.figure(figsize=(12,4))\n",
    "\n",
    "## The axial slice number (you can change this later if you like). \n",
    "slice_number = 17\n",
    "\n",
    "for ix, (img, title) in enumerate(zip([t2_img, t1_img, wl_msk], [\"T2 FLAIR\", \"T1\", \"WMLs\"])):\n",
    "\n",
    "    plot = fig.add_subplot(1,3,ix+1)\n",
    "    \n",
    "    # The colons mean that every voxel in that dimension of the array is\n",
    "    # used.  So by selecting the x and y dimensions, and specifying a slice\n",
    "    # number in z, we're making an axial slice image.\n",
    "    # Exercise: how would you make a sagittal slice?\n",
    "    implot = plot.imshow(img[:,:,slice_number], cmap=plt.cm.gray)\n",
    "\n",
    "    plt.colorbar(mappable=implot)\n",
    "    plt.title(title)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can see the White Matter Lesions in the mask and the T2 FLAIR.  To make this clearer, you can also do things like overlay the WML mask over the T2 FLAIR:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.close('all')\n",
    "\n",
    "plt.imshow(t2_img[:,:,slice_number], cmap=plt.cm.gray)\n",
    "plt.imshow(wl_msk[:,:,slice_number], cmap=plt.cm.Reds, alpha=0.5)\n",
    "plt.title(\"WMLs overlaid on the T2 FLAIR\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Looking at the FLAIR image, we can see that the WMLs are much brighter than the other tissues.  Let's plot a histogram of the voxel intensities from this slice to confirm this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## The slice intensities - we use ravel() to flatten the 256x256 voxels\n",
    "#  into one long series of numbers.\n",
    "slice_intensities = t2_img[:,:,slice_number].ravel()\n",
    "\n",
    "# This clears the matplotlib canvas.  If you don't do this in a notebook,\n",
    "# the plot can build up or end up on top of each other...\n",
    "plt.close('all')\n",
    "\n",
    "# Plot the histogram of intensities from this slice.\n",
    "intensity_histogram = plt.hist(slice_intensities, bins=100, alpha=0.3, label=\"T2 FLAIR\")\n",
    "\n",
    "# Add a label using the label information provided to the histogram.\n",
    "lg = plt.legend()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Hmm, OK, there are a lot of zeroes in there from the background, making it difficult to see any other potential peaks.  Let's mask these out and try again."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Don't worry too much about the code here for now - the np.where() in the\n",
    "# second set of square brackets just picks out values that are above 0.\n",
    "ax_slice_no_zeroes = t2_img[:,:,slice_number][np.where(t2_img[:,:,slice_number] > 0.0)]\n",
    "\n",
    "plt.close('all')\n",
    "\n",
    "intensity_histogram_no_zeroes = plt.hist(ax_slice_no_zeroes.ravel(), bins=100, alpha=0.3, label=\"T2 FLAIR (no zeroes)\")\n",
    "lg = plt.legend()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "That's better!  We can see peaks corresponding to the (fluid attenuated) CSF and what is presumably the gray and \"normal\" white matter - and in the very tail there are voxels which are presumably the white matter lesions.\n",
    "\n",
    "We can confirm this by plotting the voxel intensities of the T2 FLAIR and the WML mask:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a masked image by performing an element-wise multiplication\n",
    "# of the original T2 image and the WML mask.\n",
    "#\n",
    "# (Note to experts: this is much easier and quicker with fslmaths! ;-)\n",
    "t2_masked = np.multiply(t2_img, wl_msk)\n",
    "\n",
    "plt.close('all')\n",
    "\n",
    "h = plt.hist(t2_masked[:,:,slice_number].ravel(), bins=100, alpha=0.4, label=\"T2 FLAIR (masked)\")\n",
    "\n",
    "# Let's just plot with the log of the y values - we'll get a big peak in the zero\n",
    "# bin that we will just ignore for now.\n",
    "plt.semilogy()\n",
    "\n",
    "plt.legend()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "OK, perhaps not as clear cut as we'd think/like, but the voxels we're interested in are clearly around the 400-600 intensity range for this particular slice.  You could, in principle, try simply selecting all voxels in that intensity range and labelling these as WMLs (exercise for later - try it!) and comparing that with the ground truth mask.\n",
    "\n",
    "However, let's see if Machine Learning can do any better."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Prepare your data\n",
    "\n",
    "A _very_ simple thing we could try is clustering the T2 FLAIR voxels by intensity.  In this sense, each voxel is an example/data point in its own right, represented by a vector with a single feature - its **intensity**.\n",
    "\n",
    "Let's prepare a very simple dataset from a single slice and see what ML can do, with a few caveats:\n",
    "\n",
    "* We'll look at only one slice - which provides 256x256=65536 data points/examples.  We won't test for generality outside of this slice (even within the same image) but you could easily extend the models here to do so;\n",
    "* We'll get rid of the zeroes from the outset by explicitly excluding them from the data.  This makes a very important assumption about the data - that the zero voxels contain no relevant information.  It's like saying that the black parts of a painting are either regions that are \"black\" or contain regions with \"no light\".  The practical upshot is that if you include the zeroes, your model will try and learn about the empty space in your image.  That may or may not be what you want.  In this case, we don't want it.\n",
    "\n",
    "Anyway, we implement our own form of _sparse matrix_ for our data, recording the x, y, and intensity value for each non-zero voxel.  Our structure also allows us to include the accompanying mask value (1=WML, 0=non WML) and, as we'll see, our model predictions in the voxel's row, which let's us do some useful things.\n",
    "\n",
    "So, let's prepare our data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## The slice number (in z) of interest.\n",
    "slice_number = 17\n",
    "\n",
    "## The T2 FLAIR axial slice from the original image.\n",
    "t2_slice  = t2_img[:,:,slice_number]\n",
    "\n",
    "## Scale the intensity values to between [0, 1].\n",
    "t2_slice_ = t2_slice/np.amax(t2_slice)\n",
    "\n",
    "print(\"* The T2 FLAIR slice:\")\n",
    "print(\"* --> The shape is     %s\" % (str(t2_slice_.shape)))\n",
    "print(\"* --> The (min, max) = (%.1f, %.1f)\" % (np.amin(t2_slice_), np.amax(t2_slice_)))\n",
    "\n",
    "## How many non-zero voxels are there?\n",
    "n_non_zero_t2_voxels = np.count_nonzero(t2_slice>0.0)\n",
    "print(\"* --> Number of non-zero voxels: % 6d\" % (n_non_zero_t2_voxels))\n",
    "print(\"*\")\n",
    "\n",
    "## The T1 (registered to T2 space) axial slice.\n",
    "t1_slice  = t1_img[:,:,slice_number]\n",
    "#\n",
    "t1_slice_ = t1_slice/np.amax(t1_slice)\n",
    "\n",
    "## The WML mask - no need to scale this, of course.\n",
    "wm_slice = wl_msk[:,:,slice_number]\n",
    "\n",
    "\n",
    "## The \"sparsified\" data from the slice - created as an empty matrix to start with.\n",
    "X_sp = np.zeros( (n_non_zero_t2_voxels, 5))\n",
    "\n",
    "# Fill it with the data - in a stupidly slow way for illustrative purposes.\n",
    "row = 0\n",
    "for i in range(t2_slice.shape[1]):\n",
    "    for j in range(t2_slice.shape[0]):\n",
    "        if t2_slice[j,i] > 0.0:\n",
    "            X_sp[row][0] = int(j)        # The x position\n",
    "            X_sp[row][1] = int(i)        # The y position\n",
    "            X_sp[row][2] = t1_slice[j,i] # The T1 intensity\n",
    "            X_sp[row][3] = t2_slice[j,i] # The T2 intensity\n",
    "            X_sp[row][4] = wm_slice[j,i] # The WML mask value\n",
    "            row += 1\n",
    "            \n",
    "print(\"* Shape of the sparse matrix : %s\" % (str(X_sp.shape)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's get more of a feel for our data by plotting the T1 intensities against those of the T2 intensities. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.close('all')\n",
    "\n",
    "# Plot the voxel T1 intensities vs. T2 intensities.\n",
    "plt.scatter(X_sp[:,2], X_sp[:,3], alpha=0.01)\n",
    "plt.xlabel(\"T1 intensity\")\n",
    "plt.ylabel(\"T2 intensity\")\n",
    "plt.grid(1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There's clearly some structure here.  Let's cheat a bit and see if this structure gives us any clues about the WMLs."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.close('all')\n",
    "\n",
    "# Plot the voxels by colour based on the WML mask by setting\n",
    "# the c (colour) property of each scatter point.\n",
    "plt.scatter(X_sp[:,2], X_sp[:,3], c=X_sp[:,4], alpha=0.01)\n",
    "plt.xlabel(\"T1 intensity\")\n",
    "plt.ylabel(\"T2 intensity\")\n",
    "plt.grid(1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Interesting!  It actually appears that, in this slice at least, most of the WML voxels have a T2 intensity greater than ~410, and that, in fact, the T1 information doesn't really add much (although, perhaps interestingly, there are two sub-clusters of WML in the T1 space - but we'll leave that for now...).\n",
    "\n",
    "Let's prepare our data with only the T2 intensity information."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## The (scaled) Design Matrix (the data was scaled already).\n",
    "#\n",
    "# The T2 values are in the fourth columns (3:4 in the sparse matrix array).\n",
    "X_ = X_sp[:,3:4].reshape((n_non_zero_t2_voxels, 1))\n",
    "\n",
    "## The target labels - the fifth column of the sparse matrix.\n",
    "y = X_sp[:,4]\n",
    "\n",
    "# Split the data - randomly select half of the voxels.\n",
    "X_train, X_test, y_train, y_test = train_test_split(X_, y, test_size=0.5, random_state=42)\n",
    "\n",
    "print(\"Training data and label shapes:\\t\", X_train.shape, y_train.shape)\n",
    "\n",
    "print(\"Testing  data and label shapes:\\t\", X_test.shape, y_test.shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Create your model(s)\n",
    "\n",
    "Let's create an unsupervised k-means clustering model with four clusters (assuming voxel intensities will be split by CSF, GM, WM, and WML lesions - we don't know if this is true, of course!)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Our k-means clustering model.\n",
    "kms_clf = cluster.KMeans(n_clusters=4, random_state=42)\n",
    "\n",
    "## Fit the model to the (training) data.\n",
    "kms_clf.fit(X_train)\n",
    "\n",
    "## Get the assigned cluster indices for the training data.\n",
    "#\n",
    "# We +1 to shift the indices assigned by the clustering algorithm [0, 3]\n",
    "# to [1, 4] - you'll see why later.\n",
    "y_pred_kms = kms_clf.predict(X_).reshape((n_non_zero_t2_voxels, 1)) + 1\n",
    "\n",
    "# Add the predicted labels for the k-means clustering to the sparse matrix.\n",
    "X_sp = np.hstack([X_sp, y_pred_kms])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "An important thing to do - if you can - is look at what the clustering has done.  And with some fancy plotting magic, we can:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Here's the cool bit.  You can colour the points on a scatter plot according to\n",
    "# the labels of the mask - or the predictions - to see what's going on.\n",
    "# Let's try this for the k-means predictions.\n",
    "\n",
    "plt.close('all')\n",
    "\n",
    "# Plot the voxels by colour based on the T2-only k-means clustering.\n",
    "plt.scatter(X_sp[:,2], X_sp[:,3], c=X_sp[:,5], alpha=0.01)\n",
    "plt.xlabel(\"T1 intensity\")\n",
    "plt.ylabel(\"T2 intensity\")\n",
    "plt.grid(1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The data points' colours have been set according to the clustering index.  You can see from this how the k-means clustering is working in one dimension (the T2 intensities, i.e. the y axis). And, to be fair, it's not done a bad job in selecting the WMLs in the top cluster.  You can also the other clusters that roughly represent other tissues.  \n",
    "\n",
    "_Remember, the T1 information was **not** used in this clustering_.\n",
    "\n",
    "What happens if we do use the T1 and T2 intensities?  Let's start again with our Design Matrix and target labels."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## The (scaled) Design Matrix (the data was scaled already).\n",
    "#\n",
    "# The T1 and T2 values are the third and fourth columns of the sparse matrix (columns 2:4).\n",
    "X_ = X_sp[:,2:4]\n",
    "\n",
    "## The target labels - the fifth column of the sparse matrix.\n",
    "y = X_sp[:,4]\n",
    "\n",
    "# Split the data - randomly select half of the voxels.\n",
    "X_train, X_test, y_train, y_test = train_test_split(X_, y, test_size=0.5, random_state=42)\n",
    "\n",
    "print(\"Training data and label shapes:\\t\", X_train.shape, y_train.shape)\n",
    "\n",
    "print(\"Testing data and label shapes:\\t\", X_test.shape, y_test.shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will once again use four clusters, but this time train on the T2 and T1 intensities."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Our k-means clustering model.\n",
    "kms_clf = cluster.KMeans(n_clusters=4, random_state=42)\n",
    "\n",
    "## Fit the model to the (training) data.\n",
    "kms_clf.fit(X_train)\n",
    "\n",
    "## Get the assigned cluster indices for the training data.\n",
    "#\n",
    "# We +1 to shift the indices assigned by the clustering algorithm [0, 3]\n",
    "# to [1, 4] - you'll see why later.\n",
    "y_pred_kms = kms_clf.predict(X_sp[:,2:4]).reshape((n_non_zero_t2_voxels, 1)) + 1\n",
    "\n",
    "# Add the predicted labels for the k-means clustering to the sparse matrix.\n",
    "X_sp = np.hstack([X_sp, y_pred_kms])\n",
    "\n",
    "# Here's the cool bit.  You can colour the points on a scatter plot according to\n",
    "# the labels of the mask - or the predictions - to see what's going on.\n",
    "# Let's try this for the k-means predictions.\n",
    "\n",
    "plt.close('all')\n",
    "\n",
    "# Plot the voxels by colour based on the WML mask.\n",
    "plt.scatter(X_sp[:,2], X_sp[:,3], c=X_sp[:,6], alpha=0.01)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Ah.  By adding the T1 information, it would appear we've given the clusterer too much information - you can see how the voxels are indeed clustered in the T1-T2 space, but that this doesn't correspond to the WML voxels as we saw previously.  So, sometimes, less is more.\n",
    "\n",
    "Of course, this is just for one slice, and one set of intensity values, so a simple 1D clustering or intensity cut may not generalise well.  Let's see what an SVM trained on the T1 and T2 information can do."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Our SVM classifier object.\n",
    "clf_svm = svm.SVC(gamma='auto')\n",
    "\n",
    "# Train our model on half of the data.\n",
    "clf_svm.fit(X_train, y_train)\n",
    "\n",
    "## The predicted labels from our trained SVM\n",
    "y_pred_svm = clf_svm.predict(X_sp[:,2:4]).reshape((n_non_zero_t2_voxels, 1))\n",
    "\n",
    "# Add the SVM predictions to the sparse matrix.\n",
    "X_sp = np.hstack([X_sp, y_pred_svm])\n",
    "\n",
    "plt.close('all')\n",
    "\n",
    "# Plot the voxels by colour based on the SVM predictions.\n",
    "plt.scatter(X_sp[:,2], X_sp[:,3], c=X_sp[:,7], alpha=0.01)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And there we go!  The SVM - trained on only half the voxels - seems to be doing a better job.  But how much better?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Evaluate your model(s)\n",
    "\n",
    "Major caveats about the nature of the training and test data used - as well as a lack of cross validation so far - let's get a feel for how these models are doing.  For segmentation problems, a useful performance metric is the Dice coefficient for the two volumes being compared, which is two times the intersection of the two volumes divided by the total number of voxels in each volume.  For perfect overlap, the Dice score is 100%.\n",
    "\n",
    "First, let's visualise the mask and predicted masks for each of the methods to make sure our models are at least doing something sensible:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## The WML mask from the original slice (column 4).\n",
    "#\n",
    "# We've used numpy's sparse matrix functionality to create a dense matrix from\n",
    "# our (custom) sparse matrix by supplying the values and voxel coordinates.\n",
    "img_mask = coo_matrix((X_sp[:,4], (X_sp[:,0].astype(int), X_sp[:,1].astype(int))), shape=(256, 256))\n",
    "\n",
    "plt.close('all')\n",
    "\n",
    "plt.imshow(img_mask.toarray(), cmap=plt.cm.gray)\n",
    "c = plt.colorbar()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## The T2 k-means cluster predicted image (column 5).\n",
    "img_clus = coo_matrix((X_sp[:,5], (X_sp[:,0].astype(int), X_sp[:,1].astype(int))), shape=(256, 256))\n",
    "\n",
    "plt.close('all')\n",
    "\n",
    "plt.imshow(img_clus.toarray(), cmap=plt.cm.gray)\n",
    "c = plt.colorbar()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can make out rough correspondences to the different tissue type - but it's certainly not perfect.  Note also how the +1 shift in the indices means that the background appears as 0, i.e. nothing.\n",
    "\n",
    "The closest clustering to the WMLs appears to be the k=4 cluster, so let's use that as our predicted image:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Our k=4 cluster image.  There's probably a nicer way of doing this, but\n",
    "#  for now we will simply set non-4 voxels to zero, and k=4 voxels to 1.\n",
    "img_clus_4 = np.copy(img_clus.toarray())\n",
    "np.putmask(img_clus_4, img_clus_4<3.9, 0)\n",
    "np.putmask(img_clus_4, img_clus_4>3.9, 1)\n",
    "plt.close('all')\n",
    "\n",
    "plt.imshow(img_clus_4, cmap=plt.cm.gray)\n",
    "c = plt.colorbar()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Out of morbid curiosity, let's see how badly the T1-T2 clustering did..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## The T1-T2 k-means cluster predicted image (column 5).\n",
    "img_clus_2 = coo_matrix((X_sp[:,6], (X_sp[:,0].astype(int), X_sp[:,1].astype(int))), shape=(256, 256))\n",
    "\n",
    "plt.close('all')\n",
    "\n",
    "plt.imshow(img_clus_2.toarray(), cmap=plt.cm.gray)\n",
    "c = plt.colorbar()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "I mean, it's not terrible, but clearly not as good as the T2-only clustering.  We won't evaluate its performance here (though feel free to try!).\n",
    "\n",
    "Finally, let's do the same for the SVM predictions:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## The SVM prediction image (column 7).\n",
    "img_svm = coo_matrix((X_sp[:,7], (X_sp[:,0].astype(int), X_sp[:,1].astype(int))), shape=(256, 256)).toarray()\n",
    "\n",
    "# The SVM predictions are probabilities, so we'll select voxels with\n",
    "# a probability greater than one to binarise the mask.\n",
    "img_svm_1 = np.copy(img_svm)\n",
    "np.putmask(img_svm_1, img_svm_1>0.0, 1)\n",
    "\n",
    "plt.close('all')\n",
    "\n",
    "plt.imshow(img_svm_1, cmap=plt.cm.gray)\n",
    "c = plt.colorbar()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we have images (volumes) for each of our models, we can calculate the Dice scores with the following function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def calc_dice(mask1, mask2):\n",
    "\n",
    "    intersection = np.multiply(mask1, mask2)\n",
    "\n",
    "    num_voxels_in_intersection = np.count_nonzero(intersection)\n",
    "\n",
    "    num_voxels_in_mask1 = np.count_nonzero(mask1)\n",
    "\n",
    "    num_voxels_in_mask2 = np.count_nonzero(mask2)\n",
    "\n",
    "    dice_coefficient = (2.*num_voxels_in_intersection)/(num_voxels_in_mask1 + num_voxels_in_mask2)\n",
    "\n",
    "    print(\"* Number of voxels in the intersection   : % 6d\" % (num_voxels_in_intersection))\n",
    "    print(\"* Number of voxels in mask 1             : % 6d\" % (num_voxels_in_mask1))\n",
    "    print(\"* Number of voxels in mask 2             : % 6d\" % (num_voxels_in_mask2))\n",
    "    print(\"*\")\n",
    "    print(\"* Dice coefficient                       : % .2f %%\" % (dice_coefficient*100))\n",
    "    \n",
    "print(\"Clustering (T2 only):\")\n",
    "calc_dice(img_clus_4, img_mask.toarray())\n",
    "print(\"*\")\n",
    "\n",
    "print(\"SVM:\")\n",
    "calc_dice(img_svm_1, img_mask.toarray())\n",
    "print(\"*\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So there we go!  We can see:\n",
    "\n",
    "1. The k-means clustering (T2 only) selects more of the WML voxels than the SVM model;\n",
    "1. However, it selects too many voxels, which is what makes the Dice score so low.  You can see from the image that additional tissue is getting selected, which is adding to the denominator;\n",
    "1. The SVM is selecting fewer WML voxels (around half), but most of these are in the WML mask, so the Dice score is better.\n",
    "\n",
    "So neither model does particularly well - but there is _a lot_ we could do to refine the model."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Refining the model(s) and further work\n",
    "\n",
    "We've had a _very_ quick look at unsupervised and supervised methods for identifying White Matter Lesions in a single axial slice of one subject.  Here are a few suggestions for improving the methodology and the results - which you should have the tools to implement either on this data or your own!\n",
    "\n",
    "1. **Cross validation**: use the cross validation techniques from the previous session to get a better feel of how generalisable the models are;\n",
    "1. **MOAR DATA**: a fundamental flaw in the work we've done here is the data we've used to train and test the data - it's just one slice of one brain.  Clearly WMLs are going to look different in different circumstances.  So a first step would be use the other slices in the subject's volume, and then go on to uses slices from the other volumes.  It might be interesting to compare the T1 and T2 intensities between scans as well - do these differ, and if so, how well can/will the model generalise to different distributions?  How would you deal with scaling voxel intensities between scans?\n",
    "1. **Other information**: in all of the models looked at here, we've only been dealing with independent voxel intensities - i.e. each voxel is an entity in its own right, and in principle it doesn't know anything about any of the other voxels when it comes to training the models (it does, however, know about the relative intensity of the T1 and T2 images).  There's clearly spatial information available - i.e. the WMLs appear in clumps - can this help at all?  (If you're interested, see how [FSL's FAST](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FAST) tool does it.) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Epilogue: preparing data for Deep Learning models\n",
    "\n",
    "So, how would we go about preparing our data for Deep Learning models?  Well, you'll delighted to know we pretty much already have!  Deep Learning software such as TensorFlow and Keras can work with images in NumPy arrays - and that's what we've done too.  The trick, of course, is working out what your model needs in terms of volumes, slices, training vs. testing vs. validation sets, etc. but that will be a function of the software you're using and the model your using - which is beyond the scope of this notebook.\n",
    "\n",
    "But feel free to ask - that's the point of these workshops - thanks you for reading/listening/typing, and good luck!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
