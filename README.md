# TUSCANI Machine Learning Workshop

This repository is for the TUSCANI Machine Learning Workshop.
Here you will find workshop materials, example data,
and code you need to get started with Machine Learning for
neuroimaging.

## Installation and Setup

### Install Conda
[Conda](https://conda.io/docs/) provides an easy way of getting
Python working on your computer, allowing you to set up Python
environments with specific software requirements independent of your
native Python installation (i.e. whatever is on there already).

We recommend using Conda for this workshop.  If not installed already,
you can find instructions here:

https://conda.io/docs/user-guide/install/index.html

You'll also need to run this from a **terminal** on your machine.
If you don't know what this means, please ask us for help
or work with someone who does until you get the hang of it.


### Create a new conda environment

The following commands should create a Conda environment
with Python 3.6 and all of the software you'll need for the workshop:

```bash
# Create a directory for working in.
export WORKING_DIR=[The directory in which you will be working]
mkdir -p ${WORKING_DIR}
cd ${WORKING_DIR}

# This will create the Conda environment in your working directory, rather
# than the conda directory in your your home folder.
conda create python=3.6 matplotlib seaborn jupyter pandas xlrd scikit-learn -p ./mlpython
```


### Activate your environment
Tell your machine to use your shiny new Conda environment with this command:

```bash
source activate mlpython
```

### Add some neuroimaging packages
NiBabel isn't in the standard Conda distributions, so we need to add this after activating the environment:

```bash
conda install -c conda-forge nibabel
```


### Clone this repository

You'll need a copy of this code, which can be retrieved from the FMRIB
GitLab server with the following command:

```bash
git clone https://git.fmrib.ox.ac.uk/vaanathi/ml_worshop_tng-tuscani
cd ml_worshop_tng-tuscani
```


### Set the Jupyter Notebook server running

You can launch the Jupyter Notebook server with the following command:

```bash
jupyter notebook
```

This will launch the notebook server home in your default browser.

### Open the appropriate notebook

Once running in your browser,
you can select the appropriate notebook to open it and get going!

_Note: to run a cell in the workbook, select it and press the
Shift and Enter key_


### Finishing and tidying up

Once you're done, 
click on the "Save" icon in the top-left of the notebook
page (which is still a 3.5-inch floppy disk, aw),
close the tab(s) in your browser, return to the
terminal from which you launched the Jupyter Notebook server,
and press Ctrl+c twice to shut down the server.
You can then close the terminal.


## The workshop material

* `ML_workshop_notebook1.ipynb`: The Jupyter notebook for session 2 - an introduction to Machine Learning with (fake) Parkinson's data;
* `ML_workshop_notebook2.ipynb`: The Jupyter notebook for session 3 - an introduction to ML with (real!) imaging data;
* `augmentations.py`: A Python file with various image and volume augmentation functions (ND).


## Data

### FPAR (Fake Parkinson's Data)

This dataset can be found in `data/fpar`. You have:

* `data/fpar/demographics.xls` : an Excel spreadsheet of the demographic information associated with each subject;
* `data/fpar/idps.csv` : a Comma-Separated Value (CSV) file of Image Derived Phenotypes (IDPs) extracted from the subject's T1 and T2 FLAIR structural scans.

Can you identify the patients with Parkinson's Disease?

### BIANCA WML sample data

This dataset can be found in `data/Segmentation`.
See the workshop session 3 notebook for more information.


## Workshop Dates

* `2018-11-26`: _ML Workshop Part I - Introduction_
* `2018-12-03`: _ML Workshop Part II - Pitches_
* `2018-12-10`: _ML Workshop Part III - Group Work_
* `2018-12-17`: _ML Workshop Part IV - Group Work_

## Useful links

* [TUSCANI schedule](https://docs.google.com/spreadsheets/d/1Cmf0HuyQh7Ggk1H5OOrScyECTT5RSOUnL9W1BL35eak/edit#gid=514131059)
